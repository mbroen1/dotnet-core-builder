FROM alpine:3.12

# Configure installation path and environment variables
ARG PREFIX=/usr/local
ENV DOTNET_ROOT=$PREFIX/dotnet
ENV PATH=$PATH:$DOTNET_ROOT

# Install dependencies
RUN apk add --no-cache icu-libs krb5-libs libgcc libintl libssl1.1 libstdc++ zlib

# Download binaries
ADD https://download.visualstudio.microsoft.com/download/pr/91571c5a-2e55-4187-8774-bbbab205fac6/4d53331ec2beb5c648202cae23643686/dotnet-sdk-3.1.405-linux-musl-x64.tar.gz dotnet-sdk-3.1.405-linux-musl-x64.tar.gz

# Unpack binaries
RUN mkdir -p $DOTNET_ROOT && \
    tar zxf dotnet-sdk-3.1.405-linux-musl-x64.tar.gz -C $DOTNET_ROOT && \
    rm dotnet-sdk-3.1.405-linux-musl-x64.tar.gz
